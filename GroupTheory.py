from typing import Set

from TestingData import testMode, TestA, TestB, testPhi, testGroup
from Array import Array
from Matrix import Matrix


# Auxiliary function
def computeDistinctPrimeFactors(n):
    result = set()
    if n == 1:
        return result

    t = 2
    while t * t <= n:
        if n % t == 0:
            result.add(t)
            n /= t
        else:
            t += 1
    result.add(int(n))
    return result


# def getDivisors(n):
#     result = set()
#
#     t = 1
#     while t <= n:
#         if n % t == 0:
#             result.add(t)
#         t += 1
#     return result


def eulerPhi(n):
    P = computeDistinctPrimeFactors(n)

    phi = float(n)

    for p in P:
        phi *= (1 - 1 / p)
    # print("φ(%d) = %d" % (n, int(phi)))
    return int(phi)


def eulersTotientFunctionDisplay():
    print("Euler's Totient Function selected")
    if not testMode:
        n = input("n = ")
        n = int(n)
    else:
        n = testPhi
        print("n = %d" % n)
    P = computeDistinctPrimeFactors(n)

    phi = float(n)

    print("φ(%d) = %d" % (n, n), end="")
    for p in P:
        phi *= (1 - 1 / p)
        print(" * (1-1/%d)" % p, end="")
    print(" = %d" % int(phi))


def extendedEuclideanAlgorithm():
    print("Extended Euclidean Algorithm selected")

    if not testMode:
        InputA = input("Input a: ")
        InputA = int(InputA)
        InputB = input("Input b: ")
        InputB = int(InputB)
        print()

        if InputA > InputB:
            InputA, InputB = InputB, InputA
            print("(Note: a and b have been swapped because a < b)")
            print()
    else:
        InputA = TestA
        InputB = TestB

    a = InputA
    b = InputB

    alpha = 0
    beta = 1
    i = 0

    k = Array()

    formatA = Array()
    formatB = Array()

    formatA.append(a)
    formatB.append(b)

    while a > 0:
        k.append(b // a)
        i += 1
        rest = b % a
        b = a
        a = rest
        formatA.append(a)
        formatB.append(b)

    # formatA.append(0)
    k.append("-")
    # formatB.append(a)

    formatAB = Matrix(i + 1, 2)

    formatAB[i][0] = alpha
    formatAB[i][1] = beta
    for j in range(0, i):
        newA = alpha
        alpha = beta - k[i - j - 1] * alpha
        beta = newA
        formatAB[i - j - 1][0] = alpha
        formatAB[i - j - 1][1] = beta

    # print("As table:")
    # print("a\tb\tk\tα\tβ")
    #
    # for j in range(0, i+1):
    #     print("%s  %s  %s  %s  %s" % (formatA[j], formatB[j], k[j], formatAB[j][0], formatAB[j][1]))
    #
    # print()

    formatMat = Matrix(i + 2, 5)
    formatMat[0][0] = 'a'
    formatMat[0][1] = 'b'
    formatMat[0][2] = 'k'
    formatMat[0][3] = 'α'
    formatMat[0][4] = 'β'

    for j in range(1, i + 2):
        formatMat[j][0] = formatA[j - 1]
        formatMat[j][1] = formatB[j - 1]
        formatMat[j][2] = k[j - 1]
        formatMat[j][3] = formatAB[j - 1][0]
        formatMat[j][4] = formatAB[j - 1][1]

    print()
    Matrix.printMatAsTable(formatMat)
    print()

    print("Greatest common divisor(%d,%d) = %d * %d + %d * %d = %d" % (formatA[0], formatB[0],
                                                                       formatA[0], formatAB[0][0], formatB[0],
                                                                       formatAB[0][1],
                                                                       formatA[0] * formatAB[0][0] + formatB[0] *
                                                                       formatAB[0][1]))

    # print()
    # print("As lists:")
    # print("a: ", end='')
    # Array.printArray(formatA, i)
    #
    # print()
    # print("b: ", end='')
    # Array.printArray(formatB, i)
    #
    # print()
    # print("k: ", end='')
    # Array.printArray(k, i)
    #
    # print()
    # print("α: ", end='')
    # for j in range(0, i + 1):
    #     print("%d " % formatAB[j][0], end='')
    #
    # print()
    # print("β: ", end='')
    # for j in range(0, i + 1):
    #     print("%d " % formatAB[j][1], end='')
    # print()


def divisors(n):
    l = []
    for i in range(1, n):
        if n % i == 0:
            l.append(i)
    l.append(n)
    return l


def orderElementsDisplay(n, G):
    L = []
    k = 1
    result = 0

    while k < G and result != 1:
        result = (n ** k) % G
        k += 1
        L.append(result)

    print("<%d> = {" % n, end="")
    for i in L:
        print("%d" % i, end=", " if i != L[-1] else "")
    print("}")
    print("ord(%d) = %d" % (n, k - 1))
    print()


def superscript(n):
    return "".join(["⁰¹²³⁴⁵⁶⁷⁸⁹"[ord(c) - ord('0')] for c in str(n)])


def subscript(n):
    return "".join(["₀₁₂₃₄₅₆₇₈₉"[ord(c) - ord('0')] for c in str(n)])


def order(n, G, canPrint=False, useUnicode=False):
    phi = eulerPhi(G)
    divs = divisors(phi)

    if canPrint:
        print(("Group is Z%s" % subscript(G)) if useUnicode else ("Group is Z_%d" % G))
        print("φ(%d) = %d" % (G, phi))
        print(("Divisors%s = %s" % (subscript(phi), str(divs).replace("[", "{").replace("]", "}"))) if useUnicode else
              ("Divisors of %d = %s" % (phi, str(divs).replace("[", "{").replace("]", "}"))))
        print("n = %d" % n)

    for k in divs:
        result = (n ** k) % G
        if canPrint:
            print(("%d%s = %d" % (n, superscript(k), result)) if useUnicode else ("%d^%d = %d" % (n, k, result)))
        if result == 1:
            if canPrint:
                print("ord(%d) = %d" % (n, k))
            return k


def orderInGroup():
    print("Order of Element in Group selected:")
    G = input("Input multiplicative Group: ")
    n = input("Input Element: ")

    G = int(G)
    n = int(n)

    print(order(n, G, True, True))


def ggT(a, b):
    a = int(a)
    b = int(b)
    while a != b:
        if a > b:
            a -= b
        else:
            b -= a
    return a


def getGroupElements(G):
    elements = set()
    for i in range(1, G):
        if i != 0 and ggT(i, G) == 1:
            elements.add(i)
    return elements


def groupAnalyzer():
    print("Group Analyzer selected:")
    if not testMode:
        G = input("Input Multiplicative Group: ")
        G = int(G)
    else:
        G = testGroup

    elements = getGroupElements(G)

    generators = []

    # print(order(940, 1383))

    for e in elements:
        # print("φ(%d) = %d" % (e, eulerPhi(e)))
        if order(e, G) == eulerPhi(G):
            generators.append(e)
        orderElementsDisplay(e, G)

    if len(generators) > 0:
        print("The Multiplicative Group %d is cyclical and has the following generators:" % G)
        for e in generators:
            print("%d" % e, end=", " if e != generators[-1] else "")
    else:
        print("%d is not a cyclical group, as it doesn't have any generators" % G)
