# DS Helper

## Installation and Usage

You "install" it by running `git clone https://gitlab.com/sebi2306/DS-Helper.git`.

If you don't have python-igraph you need to install it with `pip install python-igraph`.

Then just `python main.py`.

