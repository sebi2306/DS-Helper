
class RelationalProperty:

    def __init__(self, name):
        self.name = name
        self.bool = False
        self.reason = -1

    def setReason(self, reason):
        self.reason = reason

    def getReason(self):
        return self.reason

    def set(self, bool):
        self.bool = bool

    def __bool__(self):
        return self.bool
