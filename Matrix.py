from Array import Array


class Matrix:

    def __init__(self, h, w=0):
        self.w = w
        self.h = h
        self.mat = self.genMatrix(w, h)

    def __eq__(self, other):
        for i in range(self.h):
            for j in range(self.w):
                if self.mat[i][j] != other[i][j]:
                    return False
        return True

    def __setitem__(self, i, val):
        return self.mat[i]

    def __getitem__(self, i):
        return self.mat[i]

    def __len__(self):
        return len(self.mat)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return str([str(s) for s in self.mat]).replace("'", "")

    def __add__(self, other):
        sum = Matrix(self.h, self.w)
        for i in range(self.h):
            for j in range(self.w):
                sum[i][j] = self.mat[i][j] + other[i][j]
        return sum

    def __sub__(self, other):
        diff = Matrix(self.h, self.w)
        for i in range(self.h):
            for j in range(self.w):
                diff[i][j] = self.mat[i][j] - other[i][j]
        return diff

    def __mul__(self, other):
        m = self.w
        if m != len(other):
            print("[ERROR] A's amount of columns differs to B's amount of lines")
            return self
        n = self.h
        l = len(other[0])

        C = Matrix(n, l)
        for i in range(n):
            for j in range(l):
                C[i][j] = Matrix.scalarProduct(self.mat[i], other.transposeMat()[j])
        return C

    def append(self, value):
        self.mat.append(value)

    def transposeMat(self):
        transposeHeight = self.w
        transposeWidth = self.h
        transpose = Matrix(transposeHeight, transposeWidth)
        for i in range(transposeHeight):
            for j in range(transposeWidth):
                transpose[i][j] = self.mat[j][i]
        return transpose

    @staticmethod
    def genMatrix(w, h):
        mat = [Array(w) for y in range(h)]
        return mat

    @staticmethod
    def cleanNegatives(mat):
        matHeight = len(mat)
        matWidth = len(mat[0])

        for i in range(matHeight):
            for j in range(matWidth):
                if mat[i][j] < 0:
                    mat[i][j] = 0
        return mat

    @staticmethod
    def printBeautifiedMat(mat):
        maxStrLen = Array(mat.h)
        s = ""
        # s = "+-------+-------+-------+"
        # sepDist = 6*3 + 3 (adica len)
        for i in range(mat.h):
            maxStrLen[i] = max([len(str(x)) for x in mat[i]])
        for i in range((max(maxStrLen) + 2) * mat.w + mat.w + 1):
            if i % (max(maxStrLen) + 3) == 0:
                s += '+'
            else:
                s += '-'
        s += '\n'
        for i in range(mat.h):
            s += "|"
            s += "".join(["{:^n}|".replace("n", str(max(maxStrLen) + 2)) for j in range(mat.w)])
            s += '\n'
            for i in range((max(maxStrLen) + 2) * mat.w + mat.w + 1):
                if i % (max(maxStrLen) + 3) == 0:
                    s += '+'
                else:
                    s += '-'
            s += '\n'

        s = s[:-1]

        print(s.format(*[inner for outer in mat for inner in outer]))

    @staticmethod
    def printMatAsTable(mat):
        maxStrLen = Array(mat.h)
        s = ""
        # s = "+-------+-------+-------+"
        # sepDist = 6*3 + 3 (adica len)
        for i in range(mat.h):
            maxStrLen[i] = max([len(str(x)) for x in mat[i]])
        for i in range((max(maxStrLen) + 2) * mat.w + mat.w + 1):
            if i % (max(maxStrLen) + 3) == 0:
                s += '+'
            else:
                s += '-'
        s += '\n'
        for i in range(mat.h):
            s += "|"
            s += "".join(["{:^n}|".replace("n", str(max(maxStrLen) + 2)) for j in range(mat.w)])
            s += '\n'
            if i == 0 or i == mat.h - 1:
                for i in range((max(maxStrLen) + 2) * mat.w + mat.w + 1):
                    if i % (max(maxStrLen) + 3) == 0:
                        s += '+'
                    else:
                        s += '-'
                s += '\n'

        s = s[:-1]

        print(s.format(*[inner for outer in mat for inner in outer]))

    @staticmethod
    def printMatAsRowColTable(mat):
        maxStrLen = Array(mat.h)
        s = ""

        for i in range(mat.h):
            maxStrLen[i] = max([len(str(x)) for x in mat[i]])
        maxStrLen = max(maxStrLen)
        spaces = " " * (maxStrLen + 3)

        s += spaces
        s += Matrix.matBorderStr((maxStrLen + 2) * (mat.w-1) + mat.w , maxStrLen + 3)
        s += spaces
        s += "|"
        s += "".join(["{:^n}|" for j in range(mat.w-1)]).replace("n", str(maxStrLen + 2)).format(*mat[0])
        s += '\n'
        s += Matrix.matBorderStr((maxStrLen + 2) * (mat.w) + mat.w + 1, maxStrLen + 3)

        for i in range(mat.h-1):
            s += "|{:^n}|".replace("n", str(maxStrLen + 2)).format(mat[i][0])
            s += "".join(["{:^n}|" for j in range(mat.w-1)])
            s += '\n'

            if i == mat.h - 2:
                s += Matrix.matBorderStr((maxStrLen + 2) * (mat.w) + mat.w + 1, maxStrLen + 3)

        s = s[:-1]

        s = s.replace("n", str(maxStrLen + 2))

        # print(s)
        # print([inner for outer in mat[1:] for inner in outer[1:]])
        print(s.format(*[inner for outer in mat[1:] for inner in outer[1:]]))

    @staticmethod
    def matBorderStr(length, interval):
        s = ""
        for i in range(length):
            if i % interval == 0:
                s += "+"
            else:
                s += "-"
        s += "\n"
        return s

    @staticmethod
    def printAdjMat(self):
        if not self.mat:
            print("[]")
            return

        if type(self.mat) != list:
            return

        matHeight = len(self.mat)
        matWidth = len(self.mat[0])

        print("    ", end="")
        for i in range(matWidth):
            print("%d" % (i + 1), end=" " if i < matWidth - 1 else "")
        print()

        for i in range(matHeight):
            print(str(i + 1) + ": [", end='')
            for j in range(matWidth):
                print(self.mat[i][j], end=" " if j < matWidth - 1 else "")
            print("]")
        print()

    @staticmethod
    def printRawMat(adjMat):
        length = len(adjMat)
        for i in range(length):
            for j in range(length):
                print(adjMat[i][j], end=" ")
            print()
        print()

    @staticmethod
    def copyToMatrix(R, S):
        nr = R.w
        for i in range(nr):
            for j in range(nr):
                if R[i][j]:
                    S[i][j] = 1

    @staticmethod
    def scalarProduct(u, v):
        if len(u) != len(v):
            print("[ERROR] Vectors are of different size")
        size = len(u)
        sp = 0  # = generate_array(size)
        for i in range(size):
            sp += u[i] * v[i]
        return sp
