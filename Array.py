class Array:

    def __init__(self, w=0):
        self.w = w
        self.arr = self.genArray(w)

    def __getitem__(self, i):
        return self.arr[i]

    def __setitem__(self, i, val):
        self.arr[i] = val

    def __eq__(self, other):
        return self.w == other.w and self.arr == other.arr
        # if self.w != other.w:
        #     return False
        # else:
        #     for i in range(self.w):
        #         if self.arr[i] != other.arr[i]:
        #             return False
        #     return True

    def __len__(self):
        return len(self.arr)

    def __str__(self):
        return str(self.arr)

    def __repr__(self):
        return str(self.arr)

    def append(self, value):
        self.arr.append(value)

    @staticmethod
    def fromList(l):
        fromListArr = Array(len(l))
        Array.copyToArray(l, fromListArr.arr)
        return fromListArr

    @staticmethod
    def genArray(w):
        return [0 for x in range(w)]

    @staticmethod
    def toIntArray(arr):
        intArr = Array(len(arr))
        intArr.arr = [int(x) for x in arr]
        return intArr

    @staticmethod
    def printArray(arr, size):
        for i in range(size + 1):
            print("%d " % arr[i], end="")

    @staticmethod
    def copyToArray(arr1, arr2):
        if len(arr2) < len(arr1):
            return
        for i in range(len(arr1)):
            arr2[i] = arr1[i]

    # def printArray(self):
    #     for i in range(self.w):
    #         print("%d " % self.arr[i], end="")
