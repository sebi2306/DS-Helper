from math import sqrt
from TestingData import testMode, degSeqTest
from Array import Array
from Matrix import Matrix
from igraph import Graph


def matrixToListOfEdges(mat, undirected=False):
    s = ""
    matSize = len(mat)

    for i in range(matSize):
        for j in range(matSize):
            if undirected and i < j:
                break
            if mat[i][j]:
                s += "%d %d\n" % (i + 1, j + 1)
    return s


def degreeSequenceAnalyzer():
    print("Degree Sequence Analyzer selected")
    if not testMode:
        degSeqStr = input("deg_seq = ")
    else:
        degSeqStr = degSeqTest
    degSeq = Array.toIntArray(degSeqStr.strip().replace('(', '').replace(')', '').replace('{', '').replace('}', '').split(','))
    if havelHakimi(degSeq):
        print("%s is a valid degree sequence" % degSeq)
    else:
        print("%s is not a valid degree sequence" % degSeq)
        return
    checkGraphConnected(degSeq)
    print("[Eulerian Cycle] The given degree sequence %s an eulerian cycle" % (
        "contains" if checkEulerianCircuit(degSeq) else "doesn't contain"))
    print("[Hamiltonian Cycle] The given degree sequence %s contain a hamiltonian cycle" % (
        "can" if canHaveHamiltonianCircuit(degSeq) else "cannot"))
    print("[Chromatic Number] χ(G) <= %f" % chromaticNumber(degSeq))
    adjMat = getAdjacencyMatrixForDegSeq(degSeq)
    print()
    Matrix.printAdjMat(adjMat)
    print(matrixToListOfEdges(adjMat, True))

    if testMode:
        Matrix.printRawMat(adjMat)
        print(str(adjMat))


def canHaveHamiltonianCircuit(degSeq):
    for deg in degSeq:
        if deg < len(degSeq) / 2:
            print("[Hamiltonian Cycle] Degree %d is < %d" % (deg, len(degSeq) / 2))
            return False
    return True


def checkGraphConnected(degSeq):
    s = sum(degSeq)
    if len(degSeq) > s / 2 + 1:
        print("[Connected] The given sequence cannot form a connected graph")


def getDegSeqFromAdjacencyMatrix(mat):
    matSize = len(mat)
    degSeq = Array(matSize)
    for i in range(matSize):
        for j in range(matSize):
            if mat[i][j] and i != j:
                degSeq[i] += 1
    return degSeq


def checkEulerianCircuit(degSeq):
    for deg in degSeq:
        if deg % 2 != 0:
            print("[Eulerian Cycle] Degree %d is odd" % deg)
            return False
    return True


def havelHakimi(a, canPrint=True):
    while True:
        a = sorted(a, reverse=True)
        if canPrint:
            print("[Havel-Hakimi] %s" % a)
        if a[0] == 0 and a[len(a) - 1] == 0:
            return True

        v = a[0]
        a = a[1:]

        if v > len(a):
            return False

        for i in range(v):
            a[i] -= 1

            if a[i] < 0:
                return False


def getAdjacencyMatrixForDegSeq(degSeq):
    degSeqSize = len(degSeq)
    adjMat = Matrix(degSeqSize, degSeqSize)
    cpyDegSeq = sorted(degSeq, reverse=True)
    pos = 0
    while cpyDegSeq:
        degSeq = sorted(degSeq, reverse=True)
        if not degSeq:
            break

        degSeq = degSeq[1:]

        while cpyDegSeq[pos] > 0:
            for i in range(pos + 1, degSeqSize):
                # degSeq[pos-1]-=1
                if cpyDegSeq[i] == 0:
                    continue
                cpyDegSeq[pos] -= 1
                cpyDegSeq[i] -= 1
                adjMat[pos][i] = 1
                adjMat[i][pos] = 1
                if not havelHakimi(cpyDegSeq, False):
                    adjMat[pos][i] = 0
                    adjMat[i][pos] = 0
                    cpyDegSeq[i] += 1
                    cpyDegSeq[pos] += 1
                    continue
                if cpyDegSeq[pos] == 0:
                    break
        pos += 1

    return adjMat


def chromaticNumber(degSeq):
    e = 0
    for deg in degSeq:
        e += deg
    chi = 1 / 2 + sqrt(e + 1 / 4)
    return chi
