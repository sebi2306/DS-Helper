import math


def combinatorics():
    print("Combinatorics selected")
    n = int(input("n = "))
    k = int(input("k = "))
    print("S(%d,%d) = %d" % (n, k, S(n, k)))
    print("P(%d,%d) = %d" % (n, k, P(n, k)))
    print("F(%d,%d) = %d" % (n, k, F(n, k)))


def S(n, k):
    if n == k:
        return 1
    if k == 0:
        return 0
    return S(n - 1, k - 1) + k * S(n - 1, k)


def P(n, k):
    if k == 0 and n > 0:
        return 0
    if k > n:
        return 0
    if n == k:
        return 1
    return P(n - 1, k - 1) + P(n - k, k)


def F(n, k):
    return math.factorial(k) * S(n, k)


def inclusionExclusionPrinciple():
    print("Inclusion-Exclusion Principle selected")
    upper = int(input("Upper bound of interval: "))
    nums = [input("Divisor or empty if finished: ")]
    while nums[-1]:
        nums.append(input("Divisor or empty if finished: "))
    print()
    nums = [int(x) for x in nums if x != '']
    sieveFormula(upper, nums)


def sieveFormula(upper, nums):
    interval = upper
    result = 0
    operationsStr = ""
    nrDivStr = ""
    numsMultiplied = 1

    for i in range(len(nums)):
        nrDiv = math.floor(interval / nums[i])
        result += nrDiv
        operationsStr += "+ %d " % nrDiv
        nrDivStr += "Number of multiples of %d lower in [%d] = %d\n" % (nums[i], interval, nrDiv)
        numsMultiplied *= nums[i]

    for i in range(len(nums)):
        for j in range(i+1, len(nums)):
            nrDiv = math.floor(interval / (nums[i]*nums[j]))
            result -= nrDiv
            operationsStr += "- %d " % nrDiv
            nrDivStr += "Number of multiples of %d lower in [%d] = %d\n" % (nums[i]*nums[j], interval, nrDiv)

    if len(nums) > 2:
        nrDiv = math.floor(interval / numsMultiplied)
        result += nrDiv
        operationsStr += "+ %d" % nrDiv
        nrDivStr += "Number of all numbers multiplied in [%d] = %d\n" % (interval, nrDiv)

    nrDivStr += "Number of numbers divisible by %s = %d" % (str(nums).replace(',', ' or').strip("[]"), result)

    print(operationsStr[2:] + " = " + str(result))
    print(nrDivStr)
