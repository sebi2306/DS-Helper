from Array import Array
from Matrix import Matrix
from RelationalProperty import RelationalProperty


class Relation:

    def __init__(self, mat):
        self.adjMat = mat
        self.size = len(mat)

        self.propertyReflexive = RelationalProperty("Reflexive")
        self.propertySymmetrical = RelationalProperty("Symmetrical")
        self.propertyAntiSymmetrical = RelationalProperty("AntiSymmetrical")
        self.propertyASymmetrical = RelationalProperty("ASymmetrical")
        self.propertyTransitive = RelationalProperty("Transitive")
        self.propertyTotal = RelationalProperty("Total")

        self.checkReflexive()
        self.checkSymmetrical()
        self.checkAntiSymmetrical()
        self.checkASymmetrical()
        self.checkTransitive()
        self.checkTotal()

        self.isEquivalenceRelation = self.propertyReflexive and self.propertySymmetrical and self.propertyTransitive
        self.isPartialOrder = self.propertyReflexive and self.propertyAntiSymmetrical and self.propertyTransitive

    def __eq__(self, other):
        return self.adjMat == other.adjMat

    def __len__(self):
        return len(self.adjMat)

    def __str__(self):
        relation = "{"
        for i in range(self.size):
            for j in range(self.size):
                if self.adjMat[i][j]:
                    relation += "(%d,%d)," % (i + 1, j + 1)
        relation = relation[0:-1] + "}"
        return relation

    def getAdjMat(self):
        return self.adjMat

    def getTransClosure(self):
        newR = Relation.calculateRelProd(self, self)
        newR = Relation.union(self, newR)
        if self == newR:
            return newR
        else:
            return newR.getTransClosure()

    def makeReflexive(self):
        reflMat = Matrix(self.size, self.size)
        for i in range(self.size):
            reflMat[i][i] = 1
        return Relation.union(self, Relation(reflMat))

    def checkReflexive(self):
        for i in range(self.size):
            if self.adjMat[i][i] == 0:
                self.propertyReflexive.set(False)
                self.propertyReflexive.setReason((i + 1, i + 1))
                return

        self.propertyReflexive.set(True)
        return

    def makeSymmetrical(self):
        symmMat = Matrix(self.size, self.size)
        for i in range(self.size):
            for j in range(self.size):
                if self.adjMat[i][j]:
                    symmMat[j][i] = 1
        return Relation.union(Relation(symmMat), self)

    def checkSymmetrical(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.adjMat[i][j] == 1 and self.adjMat[j][i] == 0:
                    self.propertySymmetrical.set(False)
                    self.propertySymmetrical.setReason((i + 1, j + 1, j + 1, i + 1))
                    return

        self.propertySymmetrical.set(True)
        return

    def checkAntiSymmetrical(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.adjMat[i][j] == 1 and self.adjMat[j][i] == 1 and i != j:
                    self.propertyAntiSymmetrical.set(False)
                    self.propertyAntiSymmetrical.setReason((i + 1, j + 1, j + 1, i + 1))
                    return

        self.propertyAntiSymmetrical.set(True)
        return

    def checkASymmetrical(self):
        for i in range(self.size):
            if self.adjMat[i][i] == 1:
                self.propertyASymmetrical.set(False)
                self.propertyASymmetrical.setReason((i + 1, i + 1))
                return

        if not self.propertyAntiSymmetrical:
            self.propertyASymmetrical.set(False)
            self.propertyASymmetrical.setReason(self.propertyAntiSymmetrical.getReason())
            return
        else:
            self.propertyASymmetrical.set(True)

    def checkTransitive(self):
        for a in range(self.size):
            for b in range(self.size):
                for c in range(self.size):
                    if self.adjMat[a][b] == 1 and self.adjMat[b][c] == 1:
                        if self.adjMat[a][c] == 0:
                            self.propertyTransitive.set(False)
                            self.propertyTransitive.setReason((a + 1, b + 1, b + 1, c + 1, a + 1, c + 1))
                            return

        self.propertyTransitive.set(True)
        return

    def checkTotal(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.adjMat[i][j] == 0 and self.adjMat[j][i] == 0:
                    self.propertyTotal.set(False)
                    self.propertyTotal.setReason((i + 1, j + 1, j + 1, i + 1))
                    return

        self.propertyTotal.set(True)
        return

    def printAdjMat(self):
        Matrix.printAdjMat(self.adjMat)

    @staticmethod
    def calculateRelProd(R, S):
        nr = max(R.size, S.size)
        prodRS = Matrix(nr, nr)
        for i in range(nr):
            for j in range(nr):
                if i < len(R) and i < len(S) and j < len(R) and j < len(S):
                    if R.adjMat[i][j]:
                        for k in range(len(S)):
                            if S.adjMat[j][k]:
                                prodRS[i][k] = 1

        return Relation(prodRS)

    @staticmethod
    def parseInput(input):
        input = input.strip().replace('(', '').replace(')', '').replace('{', '').replace('}', '')
        strElements = input.split(',')
        intElements = Array.toIntArray(strElements)

        matSize = max(intElements)
        adjMat = Matrix(matSize, matSize)

        for i in range(len(intElements)):
            if i % 2 == 0:
                a = intElements[i]
                b = intElements[i + 1]
                adjMat[a - 1][b - 1] = 1

        return Relation(adjMat)

    @staticmethod
    def union(R, S):
        size = max(len(R), len(S))
        unionMat = Matrix(size, size)
        for i in range(size):
            for j in range(size):
                if R.adjMat[i][j] or S.adjMat[i][j]:
                    unionMat.mat[i][j] = 1

        return Relation(unionMat)

        # printMat(adjMat)
        # print(relation)
