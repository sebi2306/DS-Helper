from Combinatorics import combinatorics, inclusionExclusionPrinciple
from Automorphisms import graphAutomorphisms, automorphismCompositions, applyAutomorphismToGraph
from GroupTheory import extendedEuclideanAlgorithm, eulersTotientFunctionDisplay, groupAnalyzer, orderInGroup
from GraphTheory import degreeSequenceAnalyzer
from RelationalTheory import relationalAnalyzer, relationalProduct

print("DS Helper")
print("Choose tool:")
print("1. Relational Analyzer")
print("2. Relational Product")
print("3. Graph Automorphisms")
print("4. Automorphism Compositions")
print("5. Apply Automorphism to Graph")
print("6. Degree Sequence Analyzer")
print("7. Extended Euclidean Algorithm")
print("8. Euler's Totient Function")
print("9. Group Analyzer")
print("10. Order of Element in Group")
print("11. Inclusion-Exclusion Principle")
print("12. Combinatorics (Stirling and P(n,k)")

options = {
    '1': relationalAnalyzer,
    '2': relationalProduct,
    '3': graphAutomorphisms,
    '4': automorphismCompositions,
    '5': applyAutomorphismToGraph,
    '6': degreeSequenceAnalyzer,
    '7': extendedEuclideanAlgorithm,
    '8': eulersTotientFunctionDisplay,
    '9': groupAnalyzer,
    '10': orderInGroup,
    '11': inclusionExclusionPrinciple,
    '12': combinatorics
}
func = options.get(input(), "Invalid option")
if callable(func):
    func()
