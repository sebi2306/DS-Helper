from Array import Array
from Matrix import Matrix
from Relation import Relation
from TestingData import testMode, automorphismTest
from igraph import Graph


class Automorphism:

    def __init__(self, arr: Array):
        if not isinstance(arr, Array):
            raise TypeError
        self.arr = arr

    def __call__(self, *args):
        if isinstance(args[0], Matrix):
            if len(args[0]) > len(self.toPermutationMatrix()):
                return self.toPermutationMatrix(len(args[0])) * args[0] \
                       * self.toPermutationMatrix(len(args[0])).transposeMat()
            else:
                return self.toPermutationMatrix() * args[0] * self.toPermutationMatrix().transposeMat()

    def __format__(self, format_spec):
        return format(str(self), format_spec)

    def __str__(self):
        s = ""
        tmp = Array.fromList(list(range(0, len(self.arr))))
        if self.arr == tmp:
            return "Id"
        else:
            visited = set()
            for i in range(len(self.arr)):
                if self.arr[i] != i and i not in visited:
                    visited.add(i)
                    j = i
                    s += "(%d" % (j + 1)
                    while self.arr[j] != i:
                        j = self.arr[j]
                        visited.add(j)
                        s += ",%d" % (j + 1)
                    s += ")"
            return s

    def __repr__(self):
        return str(self)

    def __mul__(self, other):
        if len(self.arr) < len(other.arr):
            newArr = list(range(0, len(other.arr)))
            Array.copyToArray(self.arr, newArr)
            self.arr = Array.fromList(newArr)
        elif len(other.arr) < len(self.arr):
            newArr = list(range(0, len(self.arr)))
            Array.copyToArray(other.arr, newArr)
            other.arr = Array.fromList(newArr)
        mulArr = Array(len(self.arr))
        for i in range(len(mulArr)):
            mulArr[i] = other.arr[self.arr[i]]
        return Automorphism(mulArr)

    def toPermutationMatrix(self, size=None):
        if size is None:
            size = len(self.arr)

        permutationMatrix = Matrix(size, size)
        for i in range(len(self.arr)):
            permutationMatrix[i][self.arr[i]] = 1
        for i in range(len(self.arr), size):
            permutationMatrix[i][i] = 1;
        # Matrix.printRawMat(permutationMatrix)
        return permutationMatrix

    @staticmethod
    def parseAutomorphismList(text):
        autoList = text.split(" ")
        autoList = [Automorphism.parseAutomorphism(el) for el in autoList]
        return Array.fromList(autoList)

    @staticmethod
    def parseAutomorphism(text):
        text = text.replace("(", "")
        autos = text.split(")")
        autos = autos[:-1]
        autos = [Array.toIntArray(auto.split(",")) for auto in autos]
        maxEl = max([max(cycle) for cycle in autos])
        automorphismArr = Array.fromList([-1] * maxEl)

        for cycle in autos:
            for i in range(len(cycle)):
                automorphismArr[cycle[i] - 1] = cycle[(i + 1) % len(cycle)] - 1

        for i in range(maxEl):
            if automorphismArr[i] == -1:
                automorphismArr[i] = i

        # print(str(automorphismArr) + " == [1, 2, 0, 4, 3]")

        # print("Automorphism = " + str(automorphismArr))
        return Automorphism(Array.fromList(automorphismArr))


def automorphismCompositions():
    print("Automorphism Compositions selected")
    print("Input a space separated automorphism list:")

    if not testMode:
        text = input()
    else:
        text = "(1,6)(4,3)(2,5) (8,2)(3,7)(6,5) (8,5,1,6,2)(4,3,7)"
        print(text)
    automorphisms = Automorphism.parseAutomorphismList(text)
    # print(automorphisms)
    # print(automorphisms[0] * automorphisms[1])
    # print(str(automorphisms[0]) + " == " + text)

    # print(automorphisms)
    # print([str(automorphism) for automorphism in automorphisms])

    print("Composition of automorphisms:")
    print()
    compositionMat = Matrix(len(automorphisms) + 1, len(automorphisms) + 1)

    for i in range(len(automorphisms)):
        compositionMat[i][0] = str(automorphisms[i])
        compositionMat[0][i] = str(automorphisms[i])

    for i in range(len(automorphisms)):
        for j in range(len(automorphisms)):
            compositionMat[i + 1][j + 1] = automorphisms[j] * automorphisms[i]

    # Matrix.printRawMat(compositionMat)

    Matrix.printMatAsRowColTable(compositionMat)
    # print(automorphisms[1])
    # Matrix.printAdjMat(automorphisms[1].toPermutationMatrix())


def graphAutomorphisms():
    print("Graph Automorphisms selected")

    graphStr = automorphismTest
    if not testMode:
        graphStr = input("E = ")
    else:
        print("E = %s" % graphStr)
    graph = Graph.Adjacency(list(Relation.parseInput(graphStr).makeSymmetrical().getAdjMat()))
    adj = graph.get_adjacency()
    print(adj)
    # print(Relation(adj))
    # Matrix.printAdjMat(graph.get_adjacency())
    automorphismMatrix = graph.get_automorphisms_vf2()
    rawAutomorphisms = [Automorphism(Array.fromList(a)) for a in automorphismMatrix]
    automorphisms = [str(Automorphism(Array.fromList(a))) for a in automorphismMatrix]
    print("The automorphisms for graph g are: %s" % automorphisms)
    print()
    print("Composition of automorphisms:")
    print()
    compositionMat = Matrix(len(automorphisms) + 1, len(automorphisms) + 1)
    for i in range(len(rawAutomorphisms)):
        compositionMat[i][0] = compositionMat[0][i] = str(rawAutomorphisms[i])
    for i in range(len(rawAutomorphisms)):
        for j in range(len(rawAutomorphisms)):
            compositionMat[i + 1][j + 1] = rawAutomorphisms[j] * rawAutomorphisms[i]

    Matrix.printMatAsRowColTable(compositionMat)

    # # 1,2,3 4,5
    # p = Automorphism(Array.fromList([1, 2, 0, 4, 3]))
    # a = Automorphism.parseAutomorphism("(8,2)(3,7)(6,5)")
    # b = Automorphism.parseAutomorphism("(1,6)(4,3)(2,5)")
    # print(a.arr)
    # print(b.arr)
    # print(a * b)
    # print("ma-ta")


def applyAutomorphismToGraph():
    print("Apply Automorphism to Graph selected")

    graphStr = automorphismTest
    if not testMode:
        graphStr = input("E = ")
        p = input("p() = ")
    else:
        p = "(1,6)"
        print("E = %s" % graphStr)
        print("p() = %s\n" % p)

    graph = Relation.parseInput(graphStr).makeSymmetrical().getAdjMat()
    Matrix.printAdjMat(graph)

    Matrix.printAdjMat(Automorphism.parseAutomorphism(p)(graph))
