from TestingData import testMode, relationTest
from Matrix import Matrix
from Relation import Relation


def printProperties(R):
    if R.propertyReflexive:
        print("[Reflexive] True")
    else:
        print("[Reflexive] False because (%d,%d) is not in R" % R.propertyReflexive.getReason())

    if R.propertySymmetrical:
        print("[Symmetrical] True")
    else:
        print("[Symmetrical] False because (%d,%d) is in R but (%d,%d) isn't" % R.propertySymmetrical.getReason())

    if R.propertyAntiSymmetrical:
        print("[AntiSymmetrical] True")
    else:
        print("[AntiSymmetrical] False because (%d,%d) and (%d,%d) are in R" % R.propertyAntiSymmetrical.getReason())

    if R.propertyASymmetrical:
        print("[ASymmetrical] True")
    else:
        if len(R.propertyASymmetrical.getReason()) == 2:
            print("[ASymmetrical] False because (%d,%d) is in R" % R.propertyASymmetrical.getReason())
        else:
            print("[ASymmetrical] False because (%d,%d) and (%d,%d) are in R" % R.propertyASymmetrical.getReason())

    if R.propertyTransitive:
        print("[Transitive] True")
    else:
        print(
            "[Transitive] False because (%d,%d) and (%d,%d) are in R but (%d,%d) isn't " % R.propertyTransitive.getReason())

    if R.propertyTotal:
        print("[Total] True")
    else:
        print("[Total] False because neither (%d,%d) nor (%d,%d) are in R" % R.propertyTotal.getReason())

    print()

    R.printAdjMat()
    print()


def getNrPathsMatrix(adjMat):
    oldMat = newMat = Matrix(adjMat.h, adjMat.w)
    newMat += removeLoops(adjMat)
    multMat = adjMat * adjMat
    newMat += multMat
    while newMat != oldMat:
        oldMat = newMat
        multMat *= adjMat
        newMat += multMat
    return newMat


def removeLoops(adjMat):
    size = len(adjMat)
    for i in range(size):
        adjMat[i][i] = 0
    return adjMat


def hasLoops(R):
    adjMat = R.getAdjMat()
    matSize = len(adjMat)
    nrSteps = 0
    nullMat = Matrix(matSize, matSize)
    multMat = adjMat
    while adjMat != nullMat and nrSteps < len(adjMat):
        multMat *= adjMat
        nrSteps += 1
    if multMat == nullMat:
        return False
    return True


def getTransEdges(adjMat):
    nr = len(adjMat)
    transMat = Matrix(nr, nr)
    pathsMat = getNrPathsMatrix(adjMat)
    for i in range(nr):
        for j in range(nr):
            if pathsMat[i][j] > 1:
                transMat[i][j] = 1

    return transMat


def relationalAnalyzer():
    print("Relational Analyzer selected")
    # relation = "{(1,1),(1,2),(2,3)}"
    if not testMode:
        relationStr = input("R = ")
    else:
        relationStr = relationTest
        print("R = " + relationStr)
    print("R has the following properties:")

    R = Relation.parseInput(relationStr)
    printProperties(R)

    if R.isEquivalenceRelation:
        print("R is an Equivalence Relation")
        # isEquivalenceRelation = True
    elif R.isPartialOrder:
        print("R is a Partial Order")

    transClosure = R.getTransClosure()
    print("R+ = %s" % transClosure)
    print("R+ has the following properties:")
    printProperties(transClosure)

    reflexiveTransitiveClosure = transClosure.makeReflexive()
    print("R* = %s" % reflexiveTransitiveClosure)
    print("R* has the following properties:")
    printProperties(reflexiveTransitiveClosure)

    if not R.isPartialOrder:
        if hasLoops(R):
            print("R cannot be transformed into a Hasse-Diagram because it has one or more loops")
        elif R.propertySymmetrical:
            print("R cannot be transformed into a Hasse-Diagram because it is symmetrical")
        else:
            print("The Hasse-Diagram of R has the following Adjacency Matrix:")
            print()
            Matrix.printAdjMat(
                Matrix.cleanNegatives(removeLoops(R.getAdjMat() - getTransEdges(R.getAdjMat()))))


def relationalProduct():
    print("Relational Product selected")
    R = "{(1, 3),(1, 4),(2, 3)}"
    S = "{(3, 5),(4, 5),(4, 6)}"
    if not testMode:
        R = input("R = ")
        S = input("S = ")
    else:
        print("R = " + R)
        print("S = " + S)
    R = Relation.parseInput(R)
    S = Relation.parseInput(S)

    RS = Relation.calculateRelProd(R, S)
    print("RS = %s" % RS)
